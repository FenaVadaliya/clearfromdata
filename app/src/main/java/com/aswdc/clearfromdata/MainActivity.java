package com.aswdc.clearfromdata;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    EditText etfirstname, etlastname;
    Button btnsubmit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etfirstname = findViewById(R.id.etActFirstName);
        etlastname = findViewById(R.id.etActLastName);

        btnsubmit = findViewById(R.id.btnActSubmit);

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String firstname = etfirstname.getText().toString();
                String lastname = etlastname.getText().toString();
                String concatTemp = firstname + " " + lastname;

                Intent i = new Intent(MainActivity.this, MainActivity2.class);
                i.putExtra(Intent.EXTRA_TEXT, concatTemp);
                startActivity(i);
            }
        });


    }
}